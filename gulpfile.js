var gulp = require('gulp'),
    bs = require('browser-sync').create();

gulp.task('serve', function () {
    'use strict';
    bs.init({
        server: './'
    });

    gulp.watch('*.html').on('change', bs.reload);
});

gulp.task('default', ['serve']);
