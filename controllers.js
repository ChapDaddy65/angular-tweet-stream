//inject the twitterService into the controller
app.controller('TwitterController', function($scope, $q, twitterService, $http) {

  $scope.tweets=[]; //array of tweets

   twitterService.initialize();

  //using the OAuth authorization result get the latest 20 tweets from twitter for the user
  $scope.refreshTimeline = function(maxId) {
    twitterService.getLatestTweets(maxId).then(function(data) {
      $scope.tweets = $scope.tweets.concat(data);
    },function(){
      $scope.rateLimitError = true;
    });
  }

  //when the user clicks the connect twitter button, the popup authorization window opens
  $scope.connectButton = function() {
    twitterService.connectTwitter().then(function() {
      if (twitterService.isReady()) {
        //if the authorization is successful, hide the connect button and display the tweets
        $('#connectButton').fadeOut(function(){
          $('#getTimelineButton, #signOut').fadeIn();
          $scope.refreshTimeline();
          $scope.connectedTwitter = true;
        });
      } else {

      }
    });
  }

  //sign out clears the OAuth cache, the user will have to reauthenticate when returning
  $scope.signOut = function() {
    twitterService.clearCache();
    $scope.tweets.length = 0;
    $('#getTimelineButton, #signOut').fadeOut(function(){
      $('#connectButton').fadeIn();
      $scope.$apply(function(){$scope.connectedTwitter=false})
    });
    $scope.rateLimitError = false;
  }

  //if the user is a returning user, hide the sign in button and display the tweets
  if (twitterService.isReady()) {
    $('#connectButton').hide();
    $('#getTimelineButton, #signOut').show();
    $scope.connectedTwitter = true;
    $scope.refreshTimeline();
  }

  // show or hide the tweet on stream
  $scope.toggleTweet = function(t) {
    t.isShowed = !t.isShowed;
    if (t.isShowed) {
      $http.post('https://lagoon.riffle.tv/events/twitter/show/' + t.id);
    } else {
      $http.post('https://lagoon.riffle.tv/events/twitter/hide/' + t.id);
    }
  }

});